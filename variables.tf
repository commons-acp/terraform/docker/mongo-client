
variable "container_name" {
  description = "The mongo container name"
  default = "mongoclient_container"
  type = string
}
variable "image" {
  description = "The mongoclient docker image"
  default = "mongoclient/mongoclient"
  type = string
}
variable "mongoclient_env" {
  description = "The container env variables"
  type = list
}
variable "mongoclient_port" {
  description = "The post gres exposed port"
  default = 3000
  type = number
}
variable "host_mongoclient_path" {
  description = "The mongoclient host path"
  default = "/tmp/mongoclient"
  type = string
}
variable "network_name" {
  description = "The network name"
  default = "bridge"
  type =string
}