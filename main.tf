terraform {
  required_providers {
    docker = {
      "source": "terraform-providers/docker"
    }
  }
}

data "docker_registry_image" "mongoclient" {
  name = var.image
}
resource "docker_image" "mongoclient" {
  name          = data.docker_registry_image.mongoclient.name
  pull_triggers = [data.docker_registry_image.mongoclient.sha256_digest]
  keep_locally  = true
}
resource "docker_container" "mongoclient" {
  image = docker_image.mongoclient.latest
  name  = var.container_name

  volumes {
    host_path = "${var.host_mongoclient_path}/data/"
    container_path = "/data/db mongoclient/mongoclient"
    read_only = false
  }


  ports {
    internal = 3000
    external = var.mongoclient_port
  }

  env = var.mongoclient_env


  networks_advanced {
    name = var.network_name
  }



}